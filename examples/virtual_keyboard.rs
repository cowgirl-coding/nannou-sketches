// TODO: Create your own Key enum, and tranlate nannou keys to VirtualKeyboard
// keys. Then add this module to audio_tools
use nannou::prelude::{Key};

pub enum Message {
    Pause,
    NoteOn(u8, u8),
}

pub struct VirtualKeyboard {
    octave: u8,
    velocity: u8,
}

impl VirtualKeyboard {
    pub fn new() -> VirtualKeyboard {
        VirtualKeyboard { octave: 5, velocity: 100 }
    }
    pub fn process_input(&mut self, key: Key) -> Option<Message> {
        match parse_input(key) {
            Some(instruction) => {
                match instruction {
                    Instruction::Pause => Some(Message::Pause),
                    Instruction::Semitone(x) => {
                        Some(Message::NoteOn(self.octave * 12 + x, self.velocity))
                    }
                    Instruction::OctaveDown => {
                        if self.octave > 0 { self.octave -= 1; }
                        None
                    }
                    Instruction::OctaveUp => {
                        // TODO: Allow current octave to go to 10, but don't play
                        // notes higher than 127
                        if self.octave < 9 { self.octave += 1; }
                        None
                    }
                    Instruction::VelocityDown => {
                        if self.velocity > 0 { self.velocity -= 10; }
                        None
                    }
                    Instruction::VelocityUp => {
                        if self.velocity < 120 { self.velocity += 10; }
                        None
                    }
                }
            }
            None => None
        }
    }
}

fn parse_input(key: Key) -> Option<Instruction> {
    match key {
        Key::A     => Some(Instruction::Semitone(0)),
        Key::W     => Some(Instruction::Semitone(1)),
        Key::S     => Some(Instruction::Semitone(2)),
        Key::E     => Some(Instruction::Semitone(3)),
        Key::D     => Some(Instruction::Semitone(4)),
        Key::F     => Some(Instruction::Semitone(5)),
        Key::T     => Some(Instruction::Semitone(6)),
        Key::G     => Some(Instruction::Semitone(7)),
        Key::Y     => Some(Instruction::Semitone(8)),
        Key::H     => Some(Instruction::Semitone(9)),
        Key::U     => Some(Instruction::Semitone(10)),
        Key::J     => Some(Instruction::Semitone(11)),
        Key::K     => Some(Instruction::Semitone(12)),
        Key::O     => Some(Instruction::Semitone(13)),
        Key::L     => Some(Instruction::Semitone(14)),
        Key::Z     => Some(Instruction::OctaveDown),
        Key::X     => Some(Instruction::OctaveUp),
        Key::C     => Some(Instruction::VelocityDown),
        Key::V     => Some(Instruction::VelocityUp),
        Key::Space => Some(Instruction::Pause),
        _          => None,
    }
}

enum Instruction {
    OctaveDown,
    OctaveUp,
    Pause,
    Semitone(u8),
    VelocityDown,
    VelocityUp,
}


//pub enum Key {
    //A,
    //W,
    //S,
    //E,
    //D,
    //F,
    //T,
    //G,
    //Y,
    //H,
    //U,
    //J,
    //K,
    //O,
    //L,
    //Z,
    //X,
    //C,
    //V,
    //Space,
//}
